const HTTPClient = require('./httpClient.js');
const port = 3000;
const fs = require('fs');
const express = require('express');
const app = express();
// const categories = require ('./categories.json')
app.use(express.json())
app.use(express.static('public'));

const url = 'http://jservice.io/api/category?id=';
const categoriesArray = [67, 780, 277, 223, 184, 680, 21, 309, 582, 267, 136, 249, 105, 770, 508, 561, 420, 37, 1195, 25, 897];

const categoryPromises = categoriesArray.map(id => HTTPClient(url + id))

Promise.all(categoryPromises)
    .then(categories =>
        fs.writeFileSync('./categories.json', JSON.stringify(categories))
    )

app.get('/api/category/:id', (req, res) => {
    let categories = fs.readFileSync('./categories.json');
    categories = JSON.parse(categories)
    const requestedCategory = categories.find(category => category.id == req.params.id)
    res.send(requestedCategory)
    
})

app.listen(port, ()=> console.log(`listening on port number ${port}`));